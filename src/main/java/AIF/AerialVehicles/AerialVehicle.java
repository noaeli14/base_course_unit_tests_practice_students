package AIF.AerialVehicles;

import AIF.Entities.Coordinates;
import AIF.Missions.Mission;
import AIF.Missions.MissionTypeException;


public abstract class AerialVehicle implements AerialVehicleService {
    protected String pilotName;
    protected Mission mission;
    protected int hoursOfFlightSinceLastRepair;
    protected Boolean readyToFly;

    public AerialVehicle(String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        this.pilotName = pilotName;
        this.mission = mission;
        this.hoursOfFlightSinceLastRepair  = Math.max(hoursOfFlightSinceLastRepair, 0);
        this.readyToFly = readyToFly;
    }

    @Override
    public void flyTo(Coordinates coordinates) {
        System.out.println("Flying to: " + coordinates);
    }

    @Override
    public void land() {
        System.out.println("Landing....");
    }

    @Override
    public void repair() {
        setReadyToFly(true);
        setHoursOfFlightSinceLastRepair(0);
    }

    public String getPilotName() {
        return this.pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public Mission getMission() {
        return this.mission;
    }

    public void setMission(Mission mission) throws MissionTypeException {}

    public int getHoursOfFlightSinceLastRepair() {
        return this.hoursOfFlightSinceLastRepair;
    }

    public Boolean getReadyToFly() {
        return readyToFly;
    }

    public void setHoursOfFlightSinceLastRepair(int hoursOfFlightSinceLastRepair) {
        this.hoursOfFlightSinceLastRepair = Math.max(hoursOfFlightSinceLastRepair, 0);
        //this.hoursOfFlightSinceLastRepair = hoursOfFlightSinceLastRepair;
    }

    public void setReadyToFly(Boolean readyToFly) {
        this.readyToFly = readyToFly;
    }
}
