package AIF.AerialVehicles.UAVs;

import AIF.AerialVehicles.AerialIntelligenceVehicle;
import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;
import AIF.Missions.IntelligenceMission;
import AIF.Missions.Mission;


public abstract class UAV extends AerialVehicle implements AerialIntelligenceVehicle {
    protected String sensorType;

    public UAV(String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.sensorType = sensorType;
    }

    public String hoverOverLocation(Coordinates coordinates){
        String message = "Hovering Over: " + coordinates;
        System.out.println(message);
        return message;
    }


    @Override
    public String collectIntelligence(){
        String message = getPilotName() + ": " + this.getClass().getSimpleName() +
                " Collecting Data in " + ((IntelligenceMission)this.getMission()).getRegion() +
                " with sensor type: " + getSensorType();
        System.out.println(message);
        return message;
    }

    public String getSensorType() {
        return sensorType;
    }
}
