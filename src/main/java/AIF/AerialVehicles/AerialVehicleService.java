package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public interface AerialVehicleService {
    void flyTo(Coordinates coordinates);
    void land();
    void check();
    void repair();
}
