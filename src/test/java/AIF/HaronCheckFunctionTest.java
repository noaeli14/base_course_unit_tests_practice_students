package AIF;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class HaronCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    //קבוצות שקילות
    @Test
    public void testCheckHaronEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),78);//74 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 78 should stay 78 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test
    public void testCheckHaronEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),180);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 180 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//-20
    public void testCheckHaronEquilibriumGroupsUnderGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-20);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -20 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }




    //ערכי גבול

    @Test
    public void testCheckLimitsHaronUpperBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),151);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 151 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHaronUpperBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),150);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 150 should be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHaronUpperBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),149);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 149 should stay to 149 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 149);
    }


    @Test
    public void testCheckLimitsHaronLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should stay 0 , after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHaronLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 0);
    }



    @Test
    public void testCheckLimitsHaronLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Shoval"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 sshould stay 1 after Shoval.check().", aifUtil.getAerialVehiclesHashMap().get("Shoval").getHoursOfFlightSinceLastRepair() == 1);
    }
}
