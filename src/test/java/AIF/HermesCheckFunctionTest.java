package AIF;

//incomplete ...

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;

import AIF.AerialVehicles.FighterJets.F15;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

//      Partially completed !
public class HermesCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }



    //קבוצות שקילות
    @Test//78
    public void testCheckHermesEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),78);//74 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 78 should stay 78 after Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 78);
    }


    @Test//123
    public void testCheckHermesEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),123);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 123 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//-20
    public void testCheckHermesEquilibriumGroupsUnderGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),-20);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -20 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }




    //ערכי גבול

    @Test
    public void testCheckLimitsHermesUpperBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),101);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 101 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHermesUpperBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),100);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 100 should be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHermesUpperBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),99);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 99 should stay to 99 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 99);
    }


    @Test
    public void testCheckLimitsHermesLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should stay 0 , after Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsHermesLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 shouldn't be reset to 0 Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 0);
    }



    @Test
    public void testCheckLimitsHermesLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 sshould stay 1 after Kohav.check().", aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair() == 1);
    }









}
