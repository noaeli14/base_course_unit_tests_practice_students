package AIF;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class FighterJetsVariableTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @Test
    public void testPilotNameType() {
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("F15").getPilotName());
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("F16").getPilotName());
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("Zik").getPilotName());
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("Shoval").getPilotName());
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("Kohav").getPilotName());
        assertNotNull("fail - pilot name should be string", aifUtil.getAerialVehiclesHashMap().get("Etan").getPilotName());
    }
}
