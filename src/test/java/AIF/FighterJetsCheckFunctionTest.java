package AIF;

//incomplete ...


import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;


//      Partially completed !
public class FighterJetsCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }



    @Test//23
    public void testCheckFighterJetsEquilibriumGroupsInGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),23);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 23 should stay 23 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 23);
    }

    @Test//-6
    public void testCheckFighterJetsEquilibriumGroupsUnderGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),-6);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -6 should be reset to 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckFighterJetsEquilibriumGroupsAboveGroup(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),299);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 299 should be reset to 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }



    //ערכי גבול

    @Test
    public void testCheckLimitsFighterJetsLowerBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 should be reset to 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundRight(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should bstay 1 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 1);
    }

    @Test
    public void testCheckLimitsFighterJetsLowerBoundMiddle(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 0 should bstay 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }



    @Test
    public void testCheckLimitsFighterJetsUpperBoundLeft(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),249);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 249 should stay 249 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 249);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundRight() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),251);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 251 should reset to 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsUpperBoundMiddle() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"),250);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should reset to 0 by f15.check().", aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair() == 0);
    }



}
