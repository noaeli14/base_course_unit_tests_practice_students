package AIF.SetMissionFunctionTests;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class KochavSetMissionFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testKochavExecutableMission() throws MissionTypeException {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Kohav").setMission(aifUtil.getAllMissions().get("bda"));
            aifUtil.getAerialVehiclesHashMap().get("Kohav").setMission(aifUtil.getAllMissions().get("intelligence"));
            aifUtil.getAerialVehiclesHashMap().get("Kohav").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }
}
