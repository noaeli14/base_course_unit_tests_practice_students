package AIF.SetMissionFunctionTests;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class F16SetMissionFunctionTest {


    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test
    public void testF16UnexecutableMission() throws MissionTypeException {
        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("intelligence"));
            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }

    @Test
    public void testF16ExecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("attack"));
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){
            fail();
        }
    }

}
