package AIF.SetMissionFunctionTests;



import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.AIFUtil;
import AIF.Missions.*;
import org.junit.BeforeClass;
import org.junit.Test;

//      Partially completed !!!
public class EtanSetMissionFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testEtanExecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("Etan").setMission(aifUtil.getAllMissions().get("attack"));
            aifUtil.getAerialVehiclesHashMap().get("Etan").setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test
    public void testEtanUnexecutableMission() throws MissionTypeException {
        try {
            aifUtil.getAerialVehiclesHashMap().get("Etan").setMission(aifUtil.getAllMissions().get("bda"));
            fail();
        }
        catch (MissionTypeException missionTypeException) {
            assertTrue(true);
        }

    }


}
